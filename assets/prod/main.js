/////////////AVATAR///////////

// GET TRUMP AVATAR
let trump_avatar = document.getElementById('trump_avatar');
let trump_avatar_out = document.getElementById('trump_avatar_out');

// AVATAR ANIMATION
function animate_trump(){
    // PNG 1
    setInterval(function(){
        trump_avatar.setAttribute("src", "assets/images/trump_run_sprite/trump_run_2.png");
    }, 150);
    // PNG2
    setInterval(function(){
        trump_avatar.setAttribute("src", "assets/images/trump_run_sprite/trump_run_3.png");
    }, 300);
    // PNG3
    setInterval(function(){
        trump_avatar.setAttribute("src", "assets/images/trump_run_sprite/trump_run_4.png");
    }, 450);
    // PNG4
    setInterval(function(){
        trump_avatar.setAttribute("src", "assets/images/trump_run_sprite/trump_run_5.png");
    }, 600);
    // PNG4
    setInterval(function(){
        trump_avatar.setAttribute("src", "assets/images/trump_run_sprite/trump_run_6.png");
    }, 750);
    // PNG5
    setInterval(function(){
        trump_avatar.setAttribute("src", "assets/images/trump_run_sprite/trump_run_1.png");
    }, 900);
}

// STATS
let burger_count = 0; // how many clicks on burger
let soda_count = 0; // how many clicks on soda
let twitter_count = 0; // how many clicks on twitter

let achievement_alert = document.getElementById("achievement_alert"); //Pop up for achievements

/////////// ACHIEVEMENT ALERT FEATURE INCOMING //////////

/////////// TIMER ///////////

// TIMER
let timer = 0;
function increment_seconds() {
    timer += 1;
}
// TIMER INTERVAL
let cancel = setInterval(increment_seconds, 1000);

// ACHIEVEMENTS ON TIME
function time(){
    if(timer >= 60 && timer < 150){
        document.getElementById("trump_bronze").style.filter = "blur(0px)";
        document.getElementById("lock_t1").style.display = "none";
    } else if(timer >= 150 && timer < 300){
        document.getElementById("trump_silver").style.filter = "blur(0px)";
        document.getElementById("lock_t2").style.display = "none";
    } else if(timer >= 300 && timer < 600){
        document.getElementById("trump_gold").style.filter = "blur(0px)";
        document.getElementById("lock_t3").style.display = "none";
    } else if(timer >= 600){
        document.getElementById("trump_platin").style.filter = "blur(0px)";
        document.getElementById("lock_t4").style.display = "none";
    }
}

/////////// AUDIO ///////////

// GET VOLUME BUTTON & AUDIO
let sound_icon = document.getElementById("sound_icon");
let audio = document.getElementById("audio");
audio.volume = 0.3;
let sound_power = "off";

// GET CLICK AUDIO
let click = document.getElementById("click");

// GET GAME OVER AUDIO
let game_over_sound = document.getElementById("game_over");
game_over_sound.volume = 0;
game_over_sound.duration = 3;

// VOLUME ICON CHANGE
sound_icon.addEventListener("click", function(){
    if(sound_power === "off"){
        sound_icon.setAttribute("src", "assets/images/icons/speaker.svg");
        sound_power = "on";
        // START HYMN
        audio.play();
        game_over_sound.volume = 0.2;
    } else {
        sound_icon.setAttribute("src", "assets/images/icons/mute.svg");
        sound_power = "off";
        // PAUSE HYMN
        audio.pause();
        game_over_sound.volume = 0;
    }
});

////////// DYNAMIC PROGRESS BARS - MAIN FUNCTION ///////////

function progress(){
    // CONSOL
    let input = document.getElementById("terminal");
    let output = document.getElementById("output");

    // TIMER
    increment_seconds();

    input.addEventListener("keydown", function(user_cmd){
        // ON ENTER
        if(user_cmd.keyCode === 13){
            // INPUT TO STRING

            let user_cmd = input.value.toString();
            if(user_cmd === "feed"){
                // OUTPUT COMMAND
                output.innerText = `> Vous avez nourri Trump, niveau actuel : ${progress_burger}%.`;
                // FEED COMMAND
                ++burger_count;
                if (progress_burger < 90 && progress_burger > 0){
                    progress_burger += 10;
                } else if(progress_burger === 0){
                    progress_burger += 30;
                    id_burger = setInterval(reduce_burger, 500);
                    progress_bar_burger.style.width = `${progress_burger}%`;
                    burger_width.innerHTML = `${progress_burger}%`;
                }
                /////// ACHIEVEMENTS ///////
                if(burger_count > 50 && burger_count < 100){
                    // DISPLAY ACHIEVEMENT IF > 50
                    document.getElementById("burger_bronze").style.filter = "blur(0px)";
                    document.getElementById("lock_b1").style.display = "none";
                    // DISPLAY ALERT
                    // achieve();
                } else if(burger_count >= 100 && burger_count < 250){
                    // DISPLAY ACHIEVEMENT IF > 100
                    document.getElementById("burger_silver").style.filter = "blur(0px)";
                    document.getElementById("lock_b2").style.display = "none";
                    // ...
                    // achieve();
                } else if(burger_count >= 250 && burger_count < 500){
                    // ... 250
                    document.getElementById("burger_gold").style.filter = "blur(0px)";
                    document.getElementById("lock_b3").style.display = "none";
                    // achieve();
                } else if(burger_count >= 500) {
                    // ... 500
                    document.getElementById("burger_platin").style.filter = "blur(0px)";
                    document.getElementById("lock_b4").style.display = "none";
                    // achieve();
                }
                // RESET VALUE
                input.value = "";
            } else if(user_cmd === "drink"){
                // OUTPUT COMMAND
                output.innerText = `> Vous avez abreuvé Trump, niveau actuel : ${progress_soda}%.`;
                // DRINK COMMAND
                ++soda_count;
                if (progress_soda < 90 && progress_soda > 0){
                    progress_soda += 10;
                } else if(progress_soda === 0){
                    progress_soda += 30;
                    id_soda = setInterval(reduce_soda, 500);
                    progress_bar_soda.style.width = `${progress_soda}%`;
                    soda_width.innerHTML = `${progress_soda}%`;
                }
                       
                ////// ACHIEVEMENTS //////
                if(soda_count > 50 && soda_count < 100){
                    document.getElementById("soda_bronze").style.filter = "blur(0px)";
                    document.getElementById("lock_s1").style.display = "none";
                    //achieve();
                } else if(soda_count >= 100 && soda_count < 250){
                    document.getElementById("soda_silver").style.filter = "blur(0px)";
                    document.getElementById("lock_s2").style.display = "none";
                    //achieve();
                } else if(soda_count >= 250 && soda_count < 500){
                    document.getElementById("soda_gold").style.filter = "blur(0px)";
                    document.getElementById("lock_s3").style.display = "none";
                    //achieve();
                } else if(soda_count >= 500) {
                    document.getElementById("soda_platin").style.filter = "blur(0px)";
                    document.getElementById("lock_s4").style.display = "none";
                    //achieve();
                }
                input.value = "";
            } else if(user_cmd === "tweet"){
                // OUTPUT COMMAND
                output.innerText = `> Vous avez tweeté, niveau actuel : ${progress_twitter}%.`;
                ++twitter_count;
                // TWEET COMMAND
                if (progress_twitter < 90 && progress_twitter > 0){
                    progress_twitter += 10;
                } else if(progress_twitter === 0){
                    progress_twitter += 30;
                    id_twitter = setInterval(reduce_twitter, 500);
                    progress_bar_twitter.style.width = `${progress_twitter}%`;
                    twitter_width.innerHTML = `${progress_twitter}%`;
                }
                /////// ACHIEVEMENTS ///////
                if(twitter_count > 50 && twitter_count < 100){
                    document.getElementById("twitter_bronze").style.filter = "blur(0px)";
                    document.getElementById("lock_tw1").style.display = "none";
                    //achieve();
                } else if(twitter_count >= 100 && twitter_count < 250){
                    document.getElementById("twitter_silver").style.filter = "blur(0px)";
                    document.getElementById("lock_tw2").style.display = "none";
                    //achieve();
                } else if(twitter_count >= 250 && twitter_count < 500){
                    document.getElementById("twitter_gold").style.filter = "blur(0px)";
                    document.getElementById("lock_tw3").style.display = "none";
                    //achieve();
                } else if(twitter_count >= 500) {
                    document.getElementById("twitter_platin").style.filter = "blur(0px)";
                    document.getElementById("lock_tw4").style.display = "none";
                    //achieve();
                }
                input.value = "";
            } else {
                // OUTPUT COMMAND
                output.innerText = `> Commande inconnue.`;
                input.value = "";
            }
        }
    })

    // BUTTONS
    let burger_button = document.getElementById("burger_btn");
    let soda_button = document.getElementById("soda_btn");
    let twitter_button = document.getElementById("twitter_btn");

    // STAT' BUTTONS
    let burger_icon = document.getElementById("burger_icon");
    let soda_icon = document.getElementById("soda_icon");
    let twitter_icon = document.getElementById("twitter_icon");

    // BUTTON BURGER ON CLICK
    burger_button.addEventListener('click', function(){
        ++burger_count;
        if(sound_power === "on"){
            click.play();
        }
        if (progress_burger < 90 && progress_burger > 0){
            progress_burger += 10;
        } else if(progress_burger === 0){
            progress_burger += 30;
            id_burger = setInterval(reduce_burger, 500);
            progress_bar_burger.style.width = `${progress_burger}%`;
            burger_width.innerHTML = `${progress_burger}%`;
        }

        //////// ACHIEVEMENTS /////////

        if(burger_count > 50 && burger_count < 100){
            // DISPLAY ACHIEVEMENT IF > 50
            document.getElementById("burger_bronze").style.filter = "blur(0px)";
            document.getElementById("lock_b1").style.display = "none";
            //DISPLAY ALERT
            //achieve();
        } else if(burger_count >= 100 && burger_count < 250){
            // DISPLAY ACHIEVEMENT IF > 100
            document.getElementById("burger_silver").style.filter = "blur(0px)";
            document.getElementById("lock_b2").style.display = "none";
            // ...
            //achieve();
        } else if(burger_count >= 250 && burger_count < 500){
            // ... 250
            document.getElementById("burger_gold").style.filter = "blur(0px)";
            document.getElementById("lock_b3").style.display = "none";
            //achieve();
        } else if(burger_count >= 500) {
            // ... 500
            document.getElementById("burger_platin").style.filter = "blur(0px)";
            document.getElementById("lock_b4").style.display = "none";
            //achieve();
        }
    });

    // SODA BUTTON ON CLICK
    soda_button.addEventListener('click', function(){
        ++soda_count;
        if(sound_power === "on"){
            click.play();
        }
        if (progress_soda < 90 && progress_soda > 0){
            progress_soda += 10;
        } else if(progress_soda === 0){
            progress_soda += 30;
            id_soda = setInterval(reduce_soda, 500);
            progress_bar_soda.style.width = `${progress_soda}%`;
            soda_width.innerHTML = `${progress_soda}%`;
        }

    ////// ACHIEVEMENTS ///////

        if(soda_count > 50 && soda_count < 100){
            document.getElementById("soda_bronze").style.filter = "blur(0px)";
            document.getElementById("lock_s1").style.display = "none";
            //achieve();
        } else if(soda_count >= 100 && soda_count < 250){
            document.getElementById("soda_silver").style.filter = "blur(0px)";
            document.getElementById("lock_s2").style.display = "none";
            //achieve();
        } else if(soda_count >= 250 && soda_count < 500){
            document.getElementById("soda_gold").style.filter = "blur(0px)";
            document.getElementById("lock_s3").style.display = "none";
            //achieve();
        } else if(soda_count >= 500) {
            document.getElementById("soda_platin").style.filter = "blur(0px)";
            document.getElementById("lock_s4").style.display = "none";
            //achieve();
        }
    });

    // TWITTER BUTTON ON CLICK
    twitter_button.addEventListener('click', function(){
        ++twitter_count;
        if(sound_power === "on"){
            click.play();
        }
        if (progress_twitter < 90 && progress_twitter > 0){
            progress_twitter += 10;
        } else if(progress_twitter === 0){
            progress_twitter += 30;
            id_twitter = setInterval(reduce_twitter, 500);
            progress_bar_twitter.style.width = `${progress_twitter}%`;
            twitter_width.innerHTML = `${progress_twitter}%`;
        }

        //////// ACHIEVEMENTS ////////
        if(twitter_count > 50 && twitter_count < 100){
            document.getElementById("twitter_bronze").style.filter = "blur(0px)";
            document.getElementById("lock_tw1").style.display = "none";
            //achieve();
        } else if(twitter_count >= 100 && twitter_count < 250){
            document.getElementById("twitter_silver").style.filter = "blur(0px)";
            document.getElementById("lock_tw2").style.display = "none";
            //achieve();
        } else if(twitter_count >= 250 && twitter_count < 500){
            document.getElementById("twitter_gold").style.filter = "blur(0px)";
            document.getElementById("lock_tw3").style.display = "none";
            //achieve();
        } else if(twitter_count >= 500) {
            document.getElementById("twitter_platin").style.filter = "blur(0px)";
            document.getElementById("lock_tw4").style.display = "none";
            //achieve();
        }
    });

    // BURGER
    let progress_bar_burger = document.getElementById("burger_bar");
    let burger_width = document.getElementById("burger_width");
    let progress_burger = 100;
    let id_burger = setInterval(reduce_burger, 500);

    // SODA
    let progress_bar_soda = document.getElementById("soda_bar");
    let soda_width = document.getElementById("soda_width");
    let progress_soda = 100;
    let id_soda = setInterval(reduce_soda, 500);

    // TWITTER
    let progress_bar_twitter = document.getElementById("twitter_bar");
    let twitter_width = document.getElementById("twitter_width");
    let progress_twitter = 100;
    let id_twitter = setInterval(reduce_twitter, 500);

    // BURGER REDUCE
    function reduce_burger(){
        time();
        if (progress_burger <= 0) {
            clearInterval(id_burger);
            burger_icon.setAttribute("src", "assets/images/icons/little_burger_out.png");
            is_alone();
        } else if(progress_burger >= 1){
            progress_burger -= 1;
            progress_bar_burger.style.width = `${progress_burger}%`;
            burger_width.innerHTML = `${progress_burger}%`;
            burger_icon.setAttribute("src", "assets/images/icons/little_burger.png");
        }
    }

    // SODA REDUCE
    function reduce_soda(){
        time();
        if (progress_soda <= 0) {
            clearInterval(id_soda);
            soda_icon.setAttribute("src", "assets/images/icons/little_soda_out.png");
            is_alone();
        } else if(progress_soda >= 2){
            progress_soda -= 2;
            progress_bar_soda.style.width = `${progress_soda}%`;
            soda_width.innerHTML = `${progress_soda}%`;
            soda_icon.setAttribute("src", "assets/images/icons/little_soda.png");
        }
    } 

    // TWITTER REDUCE
    function reduce_twitter(){
        time();
        if (progress_twitter <= 0) {
            clearInterval(id_twitter);
            twitter_icon.setAttribute("src", "assets/images/icons/little_twitter_out.png");
            is_alone();
        } else if (progress_twitter >= 5){
            progress_twitter -= 5;
            progress_bar_twitter.style.width = `${progress_twitter}%`;
            twitter_width.innerHTML = `${progress_twitter}%`;
            twitter_icon.setAttribute("src", "assets/images/icons/little_twitter.png");
        }
    }

    // CHECK IF ALL PROGRESS = ZERO
    function is_alone(){
        if(progress_burger === 0 && progress_soda === 0 && progress_twitter === 0){
            game_over();
        }
    }

    ///////////// GAME OVER //////////////

    function game_over(){
        // TRUMP AVATAR OUT
        trump_avatar_out.style.display = "block";
        trump_avatar.style.zIndex = "-1";
        // TIMER
        clearInterval(cancel);
        timer = 0;

        // GET MODAL & CONTENT
        let modal = document.getElementById("modal_box");
        let modal_content = document.getElementById("modal_div");
        let modal_text = document.getElementById("modal_text");
 
        // PRINT
        modal_text.innerHTML = `Donald trump a succombé... Cliquez sur <span class="text_red">"Rejouer"</span> pour relancer une partie!`;

        // GET THE SPAN WHICH CLOSE MODAL
        let retry_btn = document.getElementById("retry_btn");

        // WHEN CLICK ANYWHERE
        modal.addEventListener("click", function(){
            modal_content.style.animation =  "shakeY 0.5s ease-in";
        })

        // PLAY GAME OVER SOUND & PAUSE HYMN IF HE'S ON
        if(sound_power === "on"){
            audio.pause();
            sound_icon.setAttribute("src", "assets/images/icons/mute.svg");
            sound_power = "off";
        }

        game_over_sound.play();

        //////////// MODAL ///////////

        // WHEN GAME OVER, MODAL DISPLAY
        modal.style.display = "block";

        // WHEN CLICK BUTTON, CLOSE MODAL & RESET ALL
        retry_btn.onclick = function() {   
            // RESET TIMER
            setInterval(increment_seconds, 1000);
            // RESET AVATAR
            trump_avatar_out.style.display = "none";
            trump_avatar.style.zIndex = "1";

            // CLOSE MODAL
            modal.style.display = "none";

            // RESET PROGRESS BARS
            progress_burger += 100;
            progress_soda += 100;
            progress_twitter += 100;

            // RESET BURGER INTERVAL
            id_burger = setInterval(reduce_burger, 500);
            progress_bar_burger.style.width = `${progress_burger}%`;
            burger_width.innerHTML = `${progress_burger}%`;

            // RESET SODA INTERVAL
            id_soda = setInterval(reduce_soda, 500);
            progress_bar_soda.style.width = `${progress_soda}%`;
            soda_width.innerHTML = `${progress_soda}%`;

            // RESET TWITTER INTERVAL
            id_twitter = setInterval(reduce_twitter, 500);
            progress_bar_twitter.style.width = `${progress_twitter}%`;
            twitter_width.innerHTML = `${progress_twitter}%`;
        }
    }
}

///////////// PARTICLES INIT //////////////

// PARTICLES PARENTS
let particle_burger_box = document.getElementById("particle_burger_box");
let particle_soda_box = document.getElementById("particle_soda_box");
let particle_twitter_box = document.getElementById("particle_twitter_box");

//P ARTICLES
let particle_burger = document.createElement("div");
let particle_soda = document.createElement("div");
let particle_twitter = document.createElement("div");

// SHOW PARTICLE : TO TOP, OPACITY DOWN
function launch_burger(){
    particle_burger.style.marginTop = "100px";
    particle_burger.style.opacity = "0";
    particle_burger.style.marginLeft = "5px";
    particle_burger.style.transition = "0.6s ease-in";
}

function launch_soda(){
    particle_soda.style.marginTop = "100px";
    particle_soda.style.opacity = "0";
    particle_soda.style.marginLeft = "5px";
    particle_soda.style.transition = "0.6s ease-in";
}

function launch_twitter(){
    particle_twitter.style.marginTop = "100px";
    particle_twitter.style.opacity = "0";
    particle_twitter.style.marginLeft = "5px";
    particle_twitter.style.transition = "0.6s ease-in";
}

//////////// CREATE PARTICLES ////////////

// CREATE PARTICLE BURGER
function append_burger(){
    if(document.getElementById("particle_burger")){
        // IF PARTICLE EXISTS, REMOVE IT, AND RESET STYLES
        particle_burger_box.removeChild(particle_burger);
        reset_particle_burger();
    } else {
        // CALL LAUNCH FUNCTION
        particle_burger_box.append(particle_burger);
        particle_burger.id = "particle_burger";
        setTimeout(launch_burger, 50);
    }
}

// CREATE PARTICLE SODA
function append_soda(){
    if(document.getElementById("particle_soda")){
        // IF PARTICLE EXISTS, REMOVE IT, AND RESET STYLES
        particle_soda_box.removeChild(particle_soda);
        reset_particle_soda();
    } else {
        // CALL LAUNCH FUNCTION
        particle_soda_box.append(particle_soda);
        particle_soda.id = "particle_soda";
        setTimeout(launch_soda, 50);
    }
}

// CREATE PARTICLE TWITTER
function append_twitter(){
    if(document.getElementById("particle_twitter")){
        // IF PARTICLE EXISTS, REMOVE IT, AND RESET STYLES
        particle_twitter_box.removeChild(particle_twitter);
        reset_particle_twitter();
    } else {
        // CALL LAUNCH FUNCTION
        particle_twitter_box.append(particle_twitter);
        particle_twitter.id = "particle_twitter";
        setTimeout(launch_twitter, 50);
    }
}

//CALL THEM ON CLICk
document.getElementById("burger_btn").addEventListener("click", function(){
    append_burger();
})

document.getElementById("soda_btn").addEventListener("click", function(){
    append_soda();
})

document.getElementById("twitter_btn").addEventListener("click", function(){
    append_twitter();
})

//////////// RESET PARTICLES //////////////

// RESET BURGER
function reset_particle_burger(){
    if(particle_burger.style.opacity === "0"){
        // POSITION X RANDOM
        let random = Math.floor(Math.random()* (50 -(-50) - 1) + 1);
        // RESET STYLES
        particle_burger.style.opacity = "1";
        particle_burger.style.marginTop = "200px";
        particle_burger.style.left = `${random}px`;
        // ANIMATE
        append_burger();
    }
}

// RESET SODA
function reset_particle_soda(){
    if(particle_soda.style.opacity === "0"){
        // POSITION(X) RANDOM
        let random = Math.floor(Math.random()* (50 -(-50) - 1) + 1);
        // RESET STYLES
        particle_soda.style.opacity = "1";
        particle_soda.style.marginTop = "200px";
        particle_soda.style.left = `${random}px`;
        // ANIMATE
        append_soda();
    }
}

// RESET TWITTER
function reset_particle_twitter(){
    if(particle_twitter.style.opacity === "0"){
        // POSITION(X) RANDOM
        let random = Math.floor(Math.random()* (50 -(-50) - 1) + 1);
        // RESET STYLES
        particle_twitter.style.opacity = "1";
        particle_twitter.style.marginTop = "200px";
        particle_twitter.style.left = `${random}px`;
        // ANIMATE
        append_twitter();
    }
}

////////////// ONLOAD //////////////

// ANIMATE TRUMP
    window.onload = animate_trump();
// LAUNCH PROGRESSBAR
    window.onload = progress();
