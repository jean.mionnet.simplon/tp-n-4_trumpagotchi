# TRUMPAGOTCHI

*"Faites survivre Donald Trump le plus longtemps possible dans cet hommage au tamagotchi.*
*Essayez de remporter tout les achievements !"*

## TECHNOLOGIES UTILISEES

* HTML5/CSS3
* Bootstrap
* Javascript
* Animate.css

## SOURCES

* Toutes les icônes viennent de [flaticon](https://www.flaticon.com/) et ont été retravaillés par moi même.
* L'avatar est tiré d'une [création de Denys Almaral](https://denysalmaral.com/2017/02/joining-images-to-create-sprite-sheet-update-to-px-spritesrender-script.html)

**La maquette de ce projet est disponible sur** [figma](https://www.figma.com/file/mLcmFdFarPOoJFOmUtLl9J/Untitled?node-id=0%3A1)